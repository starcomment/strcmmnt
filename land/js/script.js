jQuery(document).ready(function (){

  $(".include-svg-all").load("files/img/sprite.svg");

  $(document).on("click", ".call_modal", function(){
    var that = $(this).data("modal-id");
    $(".modal_block").removeClass("active");
    $("#"+that).addClass("active");
    $("html").addClass("modal_active");
  });
  $(".close_modal").on("click", function(){
    $(".modal_block").removeClass("active");
    $("html").removeClass("modal_active");
  });
  $(".text-form_support").each(function(){
    var modalId = $(this).data("support-modal-id");

    $(this).wrap('<div class="text-form_support__wrap" />');

    if(modalId !== undefined){
      $(this).after('<div class="text-form_support__text call_modal" data-modal-id="' + modalId + '">' + $(this).data("support-text") + '</div>');
    }else{
      $(this).after('<div class="text-form_support__text">' + $(this).data("support-text") + '</div>');
    }
    var w = $(this).parent().children(".text-form_support__text").width() + 20;

    $(this).css("padding-right", w);
  });

  $(".menu-ic, .menu-shadow").on("click", function(){
    var $that = $(".wrapper");

    if($that.hasClass("menu_active")){
      $that.removeClass("menu_active")
      $("html").removeClass("modal_active");
    }else{
      $that.addClass("menu_active");
      $("html").addClass("modal_active");
    }
  });
});
