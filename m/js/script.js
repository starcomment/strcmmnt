
jQuery(document).ready(function (){

  $(".add-scl label").on("click", function(){
    var that = $(this);

    $(".add-scl label").removeClass("active")
    if(that.hasClass("active")){
      that.removeClass("active")
    }else{
      that.addClass("active")
    }
  });

  var timeout;

  $(".add-acc_btn").on("click", function(){
    var that = $(this),
    type = that.data("el-id"),
    el_id = "#add-acc_form__" + type,
    value = $(el_id).val(),
    nick_name = (type=="telegram") ? ("Telegram: " + value) : value,
    error = "E-mail адрес не может быть без @",
    access = "Способы получения уведомлений сохранены";

    $("#add-acc_wrap").append('<div class="item">' + nick_name + '<div class="remove"></div></div>')

    clearTimeout(timeout);
    $(".poppup-wrap").addClass("active");
    $(".poppup").removeClass("red green");
    if(type=="telegram"){
      $(".poppup").addClass("red");
      $(".poppup span").html(error);
    }else{
      $(".poppup").addClass("green");
      $(".poppup span").html(access);
    }
    timeout = setTimeout(function() {
      $(".poppup-wrap").removeClass("active");
    }, 4000);
  });


  $(".include-svg-all").load("files/img/sprite.svg");

  $(".service-list .ic, .current-list li:not(.checked_item), .nav li").on("click", function(){
    if($(this).hasClass("active")){
      $(this).removeClass("active");
    }else{
      $(this).parent().children("li:not(.service-list_drop__label)").removeClass("active");
      $(this).addClass("active");
    }
  });

  $("html").on("click", function(){
    $(".btn-block_menu").each(function(){
      var $that = $(this),
          $parent = $that.parents("li"),
          h = $that.height(),
          t = $that.offset().top - $(window).scrollTop(),
          sH = $(window).height(),
          pos = h + t - 20;

      if($that.hasClass("active")){
        $parent.addClass("active");
      }else{
        $parent.removeClass("active")
      }
      if(pos > sH){
        $that.addClass("p_bottom");
      }else{
        $that.removeClass("p_bottom");
      }
    });
  });

  $(".call_alert").on("click", function(){

    var text = $(this).data("notification-text"),
        type = $(this).data("notification-type");

    $(".notification").remove();

    $(".content").append('<div class="notification ' + type + '">' + text + '<svg class="icon"><use xlink:href="#close"></use></svg></div>');
    var elWidth = $(".notification").width() + 73;
    setTimeout(function(){
      $(".content .notification").css({"left":0,"width":elWidth}).addClass("show").delay(2000).queue(function(next){
        $(this).removeClass("show");
      });
    }, 200);
  });

  $("html").on("click", ".notification .icon", function(){
    $(this).parent().removeClass("show");
    setTimeout(function(){
      $(".notification").remove();
    }, 1000);
  });

  $(".menu-ic").on("click", function(){
    $(".header").addClass("z-i_1001");
  });
  $(".shadow").on("click", function(){
    $(".header").removeClass("z-i_1001");
  });

  $(".call_modal").on("click", function(){
    var that = $(this).data("modal-id");
    $(".modal_block").removeClass("active");
    $("#"+that).addClass("active");
    $("html").addClass("modal_active");
  });
  $(".close_modal").on("click", function(){
    $(".modal_block").removeClass("active");
    $("html").removeClass("modal_active");
  });

  $(".content .tasks-block .tasks-list li .right-block .play-ic svg").on("click", function(){
    var $parent = $(this).parents("li");

    if($parent.hasClass("active")){
      $parent.removeClass("active");
    }else{
      $parent.addClass("active");
    }
  });

  $(".drop-list_wrap .drop-list li").on("click", function(){
    var $that = $(this),
        text = $that.html(),
        $current = $that.parents(".drop-list_wrap").children(".current");

    $(".drop-list_wrap .drop-list li").removeClass("checked");
    $that.addClass("checked");
    $current.html(text);
  });

  $(".btn-change-text").on("click", function(){
    var $that = $(this),
        textOn = $that.data("on"),
        textOff = $that.data("off");

    if($that.hasClass("on")){
      $that.children(".text").html(textOff);
      $that.children("svg").children("use").attr("href", "#pause");
      $that.removeClass("on");
    }else{
      $that.children(".text").html(textOn);
      $that.children("svg").children("use").attr("href", "#play");
      $that.addClass("on");
    }
  });

  $(".checkbox.selector input").on("change", function(){
    var $that = $(this),
        elements = $that.attr("name");

    $(".checkbox:not(.selector) input[name='" + elements + "']").each(function() {
      if($that.prop("checked")){
        $(this).prop("checked", true);
        $(this).parents("li").addClass("selected");
      }else{
        $(this).prop("checked", false);
        $(this).parents("li").removeClass("selected");
      }
    });
  });

  $("#configure_access .checkbox.selector input").on("change", function(){
    var $that = $(this),
        elements = $that.attr("name"),
        l = $("#configure_access .checkbox:not(.selector) input[name='" + elements + "']").length;

    if($that.prop("checked")){
      $that.parent().children("label").html("Выбрано " + l + " упоминания");
    }else{
      $that.parent().children("label").html("Выбрать все");
    }
    $(".checkbox:not(.selector) input[name='" + elements + "']").each(function() {
      if($that.prop("checked")){
        $(this).parents(".item").addClass("active");
      }else{
        $(this).parents(".item").removeClass("active");
      }
    });
  });

  $(".task-activity .checkbox.selector input").on("change", function(){
    var $that = $(this),
        elements = $that.attr("name"),
        l = $("li.selected .checkbox:not(.selector) input[name='" + elements + "']").length;

    if(l==0){
      $that.prop("checked", false);
      $that.parent().children("label").html("Выбрать все");
    }else{
      $that.parent().children("label").html("Выбрано " + l + " упоминания");
    }
  });

  $(".btn-wrap .checkbox.selector input").on("change", function(){
    var $parent = $(this).parents(".btn-wrap");

    if($parent.hasClass("active")){
      $parent.removeClass("active");
    }else{
      $parent.addClass("active");
    }
  });

  $(".content .task-activity .title-block .btn-wrap .del").on("click", function(){
    $(".content .task-activity li .checkbox input").each(function(){
      if($(this).prop("checked")){
        $(this).click();
        $(this).parents("li").remove();
      }
    });
    $(this).parents(".btn-wrap").removeClass("active");
  });

  $(".drop-list_wrap .current, .drop-block_wrap .current").on("click", function(){
    var $parent = $(this).parent();

    if($parent.hasClass("active")){
      $parent.removeClass("active");
      if($parent.hasClass("modal")){
        $("html").removeClass("modal_active");
      }
    }else{
      $(".drop-list_wrap, .drop-block_wrap").removeClass("active");
      $parent.addClass("active");
      if($parent.hasClass("modal")){
        $("html").addClass("modal_active");
      }
    }
  });

  $('html').click(function() {
      $(".drop-list_wrap, .drop-block_wrap").removeClass("active");
  });
  $(".drop-list_wrap, .drop-block_wrap").click(function(event){
      event.stopPropagation();
  });

  $(".textNum").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
           // Allow: Ctrl+A
          (e.keyCode == 65 && e.ctrlKey === true) ||
           // Allow: Ctrl+C
          (e.keyCode == 67 && e.ctrlKey === true) ||
           // Allow: Ctrl+X
          (e.keyCode == 88 && e.ctrlKey === true) ||
           // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
  });

  $(".content .task-activity .content-block li .item-checkbox").on("change", function(){

    var $that = $(this).children("input"),
        $parent = $(this).parents("li"),
        name = $that.attr("name");

    if($that.prop("checked")){
      $parent.addClass("selected");
    }else{
      $parent.removeClass("selected");
    }

  });



  $(".content .task-activity .status .read-all").on("click", function(){
    $(".content .task-activity .content-block li").removeClass("new");
  });

  $(".content .task-activity .read").on("click", function(){
    $(this).parents("li").removeClass("new");
  });

  $(".content .task-activity .del").on("click", function(){
    var t = $(this).parents(".btn-block").find(".checkbox input").prop("checked");

    if(t){
      $(this).parents(".btn-block").find(".checkbox input").click()
    }
    $(this).parents("li").remove();
  });

  $(".content .task-activity .btn-wrap .hide_btns .read-all").on("click", function(){
    var t = $(this).parents(".btn-wrap").find(".checkbox input").prop("checked");

    $(this).parents(".task-activity").find(".checkbox:not(.selector) input:checked").parents("li").removeClass("new");
  });


  $(".minimize-btn").on("click", function(){
    var $that = $(this),
        $elId = $("#" + $that.data("elid"));

    if($elId.hasClass("show")){
      $that.html("Развернуть");
      $elId.removeClass("show")
    }else{
      $that.html("Свернуть");
      $elId.addClass("show")
    }
  });

  $(".hide_btn").on("click", function(){
    var $that = $(this),
        $elId = $("#" + $that.data("hide-el-id"));

    $that.addClass("active");
    $elId.addClass("show");
  });

  $(".show_btn").on("click", function(){
    var $that = $(this),
        $elId = $("#" + $that.data("show-el-id"));

    $elId.removeClass("active");
    $that.parents(".hide").removeClass("show");
  });

  $(".disable").each(function(){
    if($(this).hasClass("col_12")){
      $(this).wrap('<div class="disable-wrap col_12">')
    }else{
      $(this).wrap('<div class="disable-wrap">')
    }

  });

  $(".modal_user-block .item").on("click", function(){
    var $that = $(this);
        $parent = $that.parent();

    $parent.children(".item").removeClass("active");
    $that.addClass("active");
  });

  $(".ic_help .ic").each(function(){
    $(this).append('<svg><use xlink:href="#task-help" /></use></svg>')
  });

  $(".add-word-filter").each(function(){
    $(this).wrap('<div class="add-word-filter_wrap">')
  });

  $(".add-link").each(function(){
    $(this).wrap('<div class="add-link_wrap">')
    $(this).after('<div class="add-link_btn"/>');
  });

  $(document).on("keyup", ".add-word-filter_wrap", function(e){
    var $that = $(this),
        val = $that.children("textarea").val(),
        k = val.slice(-1),
        w = $that.width() - 40,
        w_auto = $that.width();

    if(k==","){
      val = val.replace(/,/g , "");
      $that.children("textarea").val('').attr('placeholder', '').height(16);

      $that.append('<span class="item">' + val + '<i></i></span>').append($that.children("textarea"));

      $that.children(".item").each(function(){
        var $item = $(this);
        if($item.width()>220){
          $item.width(177);
        }
      });

      $that.children("textarea").focus();

      $that.children(".item").each(function(){
        w = w - $(this).outerWidth();

        if(w > 212){
          $that.children(".add-word-filter").css('width', w);
        }else{
          w = $that.width() - 40;
          $that.children(".add-word-filter").css('width', '100%');
        }
      });
    }

  });
  $(document).on("keydown", ".add-word-filter_wrap", function(e){
    if(e.keyCode == 13) return false;
  });
  $(document).on("click", ".add-word-filter_wrap .item i", function(e){
    var $that = $(this).parents('.add-word-filter_wrap'),
        w = $that.width() - 40;

      $that.children("textarea").focus();

      $that.children(".item").each(function(){
        w = w - $(this).outerWidth();

        if(w > 290){
          $that.children(".add-word-filter").css('width', w);
        }else{
          w = $that.width() - 40;
          $that.children(".add-word-filter").css('width', '100%');
        }
      });

    $(this).parent().remove();
  });

  $(document).on("focusout", ".copyme", function(){
    var $that = $(this),
        $parent = $that.parent(),
        val = $that.val();

    if(val!='' && $that.is(":last-of-type")){
      $parent.append('<textarea class="text-form autosize copyme" placeholder="+ Добавить еще один ответ"/>');
    }
  });

  $(".content .connected-block.accounts-block_min li .remove").on("click", function(){
    $(this).parent().remove();
  });
  $(".modal_block .configure_access-block .set-access").on("click", function(){
    if($(this).hasClass("active")){
      $(".modal_block .configure_access-block .set-access").removeClass("active");
    }else{
      $(".modal_block .configure_access-block .set-access").removeClass("active");
      $(this).addClass("active");
    }
  });
  $(".configure_access-block").on("scroll click", function(){
    var scrll = $(document).scrollTop();

    $(".modal_block .configure_access-block .set-access.active").each(function(){
      var $that = $(this),
          left = $that.offset().left,
          top = $that.offset().top - scrll,
          wHeight = $(window).height(),
          height = $that.children(".set-access_list").height();

      if(wHeight < 780){
        $that.children(".set-access_list").css({
          "left" : left,
          "top"  : (top - height - 30)
        });
      }else{
        $that.children(".set-access_list").css({
          "left" : left,
          "top"  : top
        });
      }
    });
  });
  $(".set-access_list li").on("click", function(){
    var $that = $(this),
        val = $that.data('val'),
        $parent = $that.parents('.set-access');

    $parent.find("li").removeClass("select");
    $that.addClass("select");
    $parent.find('.current').html(val)
  });
  $(".modal_block .configure_access-block .item input").on("change", function(){
    if($(this).prop("checked")){
      $(this).parents(".item").addClass("active");
    }else{
      $(this).parents(".item").removeClass("active");
      $(this).parents(".item").find(".set-access").removeClass("active");
    }
  });

  // $('.autosize').autoResize();f
});
